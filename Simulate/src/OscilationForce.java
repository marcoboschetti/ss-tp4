import java.util.List;

/**
 * K : Constante del resorte
 * Gamma : Constante de amortiguamiento
 */
public class OscilationForce implements ForceCalculator {

    private double k, gamma;

    public OscilationForce(double k, double gamma) {
        this.k = k;
        this.gamma = gamma;
    }


    @Override
    public void calculateForce(List<Particle> p, List<Vector> forces) {
        for (int i = 0; i < p.size(); i++) {
            forces.get(i).setX((-1) * k * p.get(i).getX() - gamma * p.get(i).getVelX());
            forces.get(i).setY((-1) * k * p.get(i).getY() - gamma * p.get(i).getVelY());

        }
    }

    public Vector calculateForce(Particle p) {

            double forceX = (-1) * k * p.getX() - gamma * p.getVelX();
            double forceY = (-1) * k * p.getY() - gamma * p.getVelY();
            return new Vector(forceX,forceY);
    }
}
