import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Created by tritoon on 10/08/16.
 */


public class Simulator {

    private static final int STEPS = 10;
    private static final double MARS_PROXIMITY_LIMIT = 0;
    //    private static final double MARS_PROXIMITY_LIMIT = 3389.9 * 1000 * 20;
    private static final double GALAXY_RADUIS = Math.hypot(0.831483493435295 * Math.pow(10, 8) * 1000, 1.914579540822006 * Math.pow(10, 8) * 1000) * 2;
    private static final boolean PRINTING = true;
    private final double SAFE_DISTANCE_TO_SUN = 1.391734353396533E8 * 0.05;
    private static final boolean borderCondition = true;
    private List<Particle> particles;
    private Particle earth, mars, sun;

    public List<double[]> simulate(double launchingTime,
                                   double timeDelta,
                                   double printDelta,
                                   TemporalUnit launchIntervalUnit, int launchIntervalMultiplier,
                                   TemporalUnit offsetUnit, int offset,
                                   TemporalUnit postLaunchingUnit, int postLaunchingMultiplier,
                                   String fileXYZ) {


        double launchInterval = launchIntervalMultiplier * launchIntervalUnit.getDuration().get(ChronoUnit.SECONDS);
        double offsetSeconds = offset * offsetUnit.getDuration().get(ChronoUnit.SECONDS);
        double postLaunchingSeconds = postLaunchingMultiplier * postLaunchingUnit.getDuration().getSeconds();

        //   ForceCalculator f = initializeOscilator();
        //ForceCalculator f = initializeDots();

        PrintWriter writer = null;


        double minAngle = -Math.PI / 2;
        double maxAngle = Math.PI / 2;

        double successRate = 0;
        double[] bestApproach = {5.24E+07, 6.04E+07, 0, 6.04E+07, 0.942477796};

        double[] spacheship1 = {5.21064E7, 1.0125797657111004};
        double[] spacheship2 = {5.39142E7, 0.5617955575286686};

        List<double[]> samples = new LinkedList<>();
        samples.add(spacheship1);
        samples.add(spacheship2);

        while (successRate < 0.8) {
            List<Vector> forces = new ArrayList<>();
            IntegrationScheme integration = new LeapFrogMethod();
            List<Particle> corners = new ArrayList<>();
            particles = new ArrayList<>();
            List<SpaceShip> spaceShips = new ArrayList<>();
            ForceCalculator f = initializeSolarSystem();

//            String fileName = String.format("MinAngle%fMaxAngle%fOffset%fLaunchingTime%flaunchInterval%fpostLaunchingTime%ftimeDelta%f",
//                    minAngle, maxAngle, offsetSeconds, launchingTime, launchInterval, postLaunchingSeconds, timeDelta);
            String fileName = "fullRangeAnimation";

            if (PRINTING) {
                PrinterXYZ.startPrinting(String.format("%s.xyz", fileName));
                PrinterXYZ.printTimeStep(particles, corners, 0);
            }

            for (int i = 0; i < particles.size(); i++) {
                forces.add(new Vector(0, 0));
            }
            double nextLaunch = offsetSeconds;
            double timerCounter = 0;
            List<double[]> results = new LinkedList<>();
            List<double[]> guesses = new LinkedList<>();
            double deltaAngle = maxAngle - minAngle;
            double angleStep = deltaAngle / STEPS;

            System.out.println(String.format("MinAngle: %f MaxAngle: %f Offset: %f MaxTime: %f launchInterval: %f timeDelta: %f",
                    minAngle, maxAngle, offsetSeconds, launchingTime, launchInterval, timeDelta));
            System.out.println("Best approach time: " + bestApproach[3] + " distance: " + bestApproach[2]);
            for (double totalTime = 0; totalTime < launchingTime + offsetSeconds + postLaunchingSeconds; totalTime += timeDelta) {

                if (nextLaunch <= 0 && totalTime <= offsetSeconds + launchingTime) {
                    for (double i = minAngle; i <= maxAngle; i += angleStep) {
                        launchSpaceShip(particles, spaceShips, totalTime, i);
                        forces.add(new Vector(0, 0));
                    }
                    System.out.println("size: " + particles.size());
//                    forces.add(new Vector(0, 0));
//                    launchSpaceShip(particles, spaceShips, totalTime, 0);

                    nextLaunch = launchInterval;
                }

//                final double totalTimeAux = totalTime;
//                samples.stream().filter(x-> x[0] == totalTimeAux).forEach(x -> {
//                    launchSpaceShip(particles, spaceShips, totalTimeAux, x[1]);
//                    forces.add(new Vector(0,0));
//                });

                areWeMartiansYet(mars, spaceShips, forces, results, guesses, totalTime);

                f.calculateForce(particles, forces);

                for (int i = 0; i < particles.size(); i++) {
                    Vector newForce = forces.get(i);
                    integration.updateParticle(particles.get(i), timeDelta, newForce.getX(), newForce.getY());
                }

                timerCounter += timeDelta;
                nextLaunch -= timeDelta;

                if (timerCounter >= printDelta) {
                    if (PRINTING) {
                        PrinterXYZ.printTimeStep(particles, corners, 0);
                        ColorManager.setSpeedColor(spaceShips);
                    }
//                    PrinterXYZ.printTimeStep(particles, corners, 0);
                    timerCounter = 0;
                    if (totalTime < offsetSeconds) {
                        System.out.println("pre: " + (0.0 + totalTime / offsetSeconds) + "%");
                    } else if (totalTime < offsetSeconds + launchingTime) {
                        System.out.println("launching: " + ((0.0 + totalTime - offsetSeconds) / launchingTime) + "%");
                    } else {
                        System.out.println("post: " + ((0.0 + totalTime - offsetSeconds - launchingTime) / postLaunchingSeconds) + "%");
                    }
                }
            }
            if (PRINTING) {
                PrinterXYZ.endPrinting();
            }
            System.out.println("Processing data...");
            for (SpaceShip spaceShip : spaceShips) {
                double[] entry = {spaceShip.getLaunchTime(), -1, spaceShip.getMinDistanceToMars(), spaceShip.getLandingAproxTime(), spaceShip.getLaunchAngle(), -1};
                results.add(entry);
            }

//            String fileName = String.format("Angle0Offset%fMaxTime%flaunchInterval%ftimeDelta%f",
//                    offsetSeconds, launchingTime, launchInterval, timeDelta);

            try {
                writer = new PrintWriter(fileName + "Arrivals.csv", "UTF-8");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            writer.write("Landings\nDeparture,Arrival, Angle, ArrivalVelocity\n");
            double minOverallDistance = results.get(0)[2];
            for (double[] d : results) {
                writer.write(d[0] + "," + d[1] + "," + d[4] + "," + d[5] + "\n");
                if (d[2] < minOverallDistance) {
                    minOverallDistance = d[2];
                }
            }
            writer.close();
            ArrayList<PriorityQueue<double[]>> listByAngle = new ArrayList<>(STEPS + 1);
            /*
            for (double i = minAngle; i <= maxAngle; i += angleStep) {
                listByAngle.add(new PriorityQueue<double[]>((x, y) -> (int) Math.signum(x[2] - y[2])));// TODO: 24/9/2016 check order
            }
            int arrivals = 0;
            for (double[] entry : results) {
                int index = (int) Math.round((entry[4] - minAngle) * STEPS / (maxAngle - minAngle));
                if (entry[2] < minOverallDistance * 4)
                    listByAngle.get(index).offer(entry);
                if (entry[1] > 0)
                    arrivals++;
            }
            System.out.println("Spaceships: " + spaceShips.size());
            successRate = ((double) arrivals) / results.size();
            bestApproach[2] = -1;
            for (PriorityQueue<double[]> pq : listByAngle) {
                System.out.println("size: " + pq.size());
                double[] first = null;
                double[] second;
                double[] third;
                if (!pq.isEmpty()) {
                    first = pq.poll();
//                    if (!pq.isEmpty()) {
//                        second = pq.poll();
//                        if (second[3] < first[3]) {
//                            first = second;
//                        }
//                        if (!pq.isEmpty()) {
//                            third = pq.poll();
//                            if (third[3] < first[3]) {
//                                first = third;
//                            }
//                        }
//                    }
                }
                if (first != null && (bestApproach[2] > first[2] || bestApproach[2] == -1)) {
                    bestApproach = first;
                }
            }
            */
            bestApproach = results.stream().sorted((x, y) -> (int) Math.signum(x[2] - y[2])).findFirst().get();
//            if (bestApproach[1] > 0)
            successRate = 1;
            maxAngle = bestApproach[4] + deltaAngle * 0.9 / 2;
            minAngle = bestApproach[4] - deltaAngle * 0.9 / 2;
//            timeDelta = timeDelta * 0.9 < 100 ? 100 : timeDelta * 0.9;
            launchingTime *= 0.9;
            offsetSeconds = Math.max(bestApproach[0] - launchingTime / 2, 0);
            postLaunchingSeconds = (bestApproach[3] + launchingTime / 2) - (offsetSeconds + launchingTime);
            launchInterval *= 0.9;
            System.out.println("Finished processing, success rate(comparing by distance): " + successRate);

        }


        return null;
    }

    private Particle launchSpaceShip(List<Particle> particles, List<SpaceShip> spaceShips, double totalTime) {
        return launchSpaceShip(particles, spaceShips, totalTime, Math.PI / 2);
    }

    private Particle launchSpaceShip(List<Particle> particles, List<SpaceShip> spaceShips, double totalTime, double launchAngle) {
        final double launchDistance = 1000 * 1500;
        final double launchVel = 11 * 1000;
//        final double launchVel = 7.21 * 1000 + 3 * 1000;

        double angle = Math.atan2(earth.getY() - sun.getY(), earth.getX() - sun.getX());
        double launchX = Math.cos(angle) * (launchDistance + earth.getRadius()) + earth.getX();
        double launchY = Math.sin(angle) * (launchDistance + earth.getRadius()) + earth.getY();
        double velAngle = angle + launchAngle;
        double launchVelX = Math.cos(velAngle) * launchVel + earth.getVelX();
        double launchVelY = Math.sin(velAngle) * launchVel + earth.getVelY();
        double squareDistanceToMars = (launchX - mars.getX()) * (launchX - mars.getX()) + (launchY - mars.getY()) * (launchY - mars.getY());
        double distanceToSun = Math.sqrt(launchX - sun.getX() * (launchX - sun.getX()) + (launchY - sun.getY()) * (launchY - sun.getY()));

        SpaceShip spaceShip = new SpaceShip(launchX, launchY, launchVelX, launchVelY, totalTime, squareDistanceToMars, launchAngle);
        particles.add(spaceShip);
        spaceShips.add(spaceShip);
        return spaceShip;
    }

    private OscilationForce initializeOscilator() {
        double k = Math.pow(10, 4);
        double gamma = 100;
        particles.add(new Particle(1, 0, (-1) * gamma / (2 * 70), 0, 0, 70));
        return new OscilationForce(k, gamma);

    }

    private void areWeMartiansYet(Particle mars, List<SpaceShip> spaceShips, List<Vector> forces, List<double[]> results, List<double[]> guesses, double currentTime) {
        List<SpaceShip> toRemove = new LinkedList<>();
        for (SpaceShip spaceShip : spaceShips) {

            // -2  =  awesome sun tanning =D
            if (checkLanding(sun, spaceShip, SAFE_DISTANCE_TO_SUN)) {
                toRemove.add(spaceShip);
                double[] vector = {spaceShip.getLaunchTime(), -2, spaceShip.getMinDistanceToMars(), spaceShip.getLandingAproxTime(), spaceShip.getLaunchAngle(), -1};
                results.add(vector);
                System.out.println("Burned!");
            } else if (checkLanding(mars, spaceShip, MARS_PROXIMITY_LIMIT)) {
                System.out.println("Mars Landing!");
                toRemove.add(spaceShip);
                double[] vector = {spaceShip.getLaunchTime(), currentTime, spaceShip.getMinDistanceToMars(),
                        spaceShip.getLandingAproxTime(), spaceShip.getLaunchAngle(), Math.hypot(spaceShip.getVelX(), spaceShip.getVelY())};
                results.add(vector);
            } else if (isTooFar(spaceShip, currentTime, GALAXY_RADUIS)) {
                toRemove.add(spaceShip);
                double[] vector = {spaceShip.getLaunchTime(), -3, spaceShip.getMinDistanceToMars(), spaceShip.getLandingAproxTime(), spaceShip.getLaunchAngle(), -1};
                results.add(vector);
                System.out.println("In the dark side");
            } else {
                spaceShip.updateDistance(mars, currentTime);
            }
        }
        for (Particle particle : toRemove) {
            forces.remove(particles.indexOf(particle));
            particles.remove(particle);
        }
        spaceShips.removeAll(toRemove);

//        int size = spaceShips.size()/(STEPS + 1);
//        for (int i = 0; i < size; i++) {
//            for (int angle =i*(STEPS+1); angle < STEPS - 1; angle++){
//                if (interpolate(spaceShips.get(angle), spaceShips.get(angle + 1))) {
//                    double[] entry = {spaceShips.get(angle).getLaunchAngle(), spaceShips.get(angle+1).getLaunchAngle(), currentTime};
//                    guesses.add(entry);
//                }
//            }
//        }
    }

    private boolean isTooFar(SpaceShip spaceShip, double currentTime, double galaxyRadius) {
        double xDist = spaceShip.getX();
        double yDist = spaceShip.getY();
        double sqrDist = xDist * xDist + yDist * yDist;

//
        return (currentTime - spaceShip.getLaunchTime() > 365 * 24 * 60 * 60) || sqrDist > galaxyRadius * galaxyRadius;
    }

    private boolean interpolate(Particle p1, Particle p2) {
        double maxX = Math.max(p1.getX(), p2.getX());
        double maxY = Math.max(p1.getY(), p2.getY());
        double minX = Math.min(p1.getX(), p2.getX());
        double minY = Math.min(p1.getY(), p2.getY());
        if (mars.getX() < maxX && mars.getX() > minX && mars.getY() < maxY && mars.getY() > minY) {
            return true;
        }
        return false;
    }

    private boolean checkLanding(Particle planet, SpaceShip spaceShip, double landingDistance) {
        double xDist = planet.getX() - spaceShip.getX();
        double yDist = planet.getY() - spaceShip.getY();
        double sqrDist = xDist * xDist + yDist * yDist;
//        System.out.println("sqrt to sun: " + sqrDist);
//        System.out.println("min distance sqrt: " + ((planet.getRadius() + Math.sqrt(landingDistance)) + spaceShip.getRadius()) * ((planet.getRadius() + Math.sqrt(landingDistance)) + spaceShip.getRadius()));
//        System.out.println("min distance: " + Math.sqrt(((planet.getRadius() + Math.sqrt(landingDistance)) + spaceShip.getRadius()) * ((planet.getRadius() + Math.sqrt(landingDistance)) + spaceShip.getRadius())));
//        System.out.println("earth dist: " + Math.sqrt(earth.getX() * earth.getX() + earth.getY() * earth.getY()));
//        System.out.println("sqrt corrected:" + distanceToSun * distanceToSun);
//        System.out.println("distance to sun:" + distanceToSun);


        return sqrDist <= ((planet.getRadius() + landingDistance) + spaceShip.getRadius()) * ((planet.getRadius() + landingDistance) + spaceShip.getRadius());
    }

    private ForceCalculator initializeDots() {
        particles.add(new Particle(10, 0, 0, 0.01, 1, 0.00001));
        particles.add(new Particle(0, 0, 0, 0, 1, 9999999));

        return new GravityForce();
    }

    private ForceCalculator initializeSolarSystem() {
         /* MARS
        X			Y			Z
        0.831483493435295E8	-1.914579540822006E8	-0.000000000000000 [km]
        VX			VY			VZ
        23.637912321314047	11.429021426712032	-0.000029527542055 [km/s]

        mean radius: 3389.9 km
        mass: 6.4185E23 kg
        */

        mars = new Particle(0.831483493435295 * Math.pow(10, 8) * 1000, -1.914579540822006 * Math.pow(10, 8) * 1000,
                23.637912321314047 * 1000, 11.429021426712032 * 1000, 3389.9 * 1000, 6.4185 * Math.pow(10, 23));

        /* EARTH
        * X			Y			Z
        1.391734353396533E8	-0.571059040560652E8	-0.000000000000000E8 [km]
        VX			VY			VZ
        10.801963811159256	27.565215006898345	-0.001128630342716   [km/s]

        mean radius: 6371 km
        mass: 5.972E24 kg
        */
        earth = new Particle(1.391734353396533 * Math.pow(10, 8) * 1000, -0.571059040560652 * Math.pow(10, 8) * 1000,
                10.801963811159256 * 1000, 27.565215006898345 * 1000, 6371 * 1000, 5.972 * Math.pow(10, 24));

        /* SUN
       X			Y			Z
        0			0			0
        VX			VY			VZ
        0			0			0

        mean radius: 695700 km
        mass: 1.988E30 kg
        */
        sun = new Particle(0, 0, 0, 0, 695700 * 1000, 1.988 * Math.pow(10, 30));

        particles.add(earth);
        particles.add(sun);
        particles.add(mars);

        ColorManager.marsLanding(particles);
        return new GravityForce();
    }

    List<Vector> compareOscillation(double MAX_TIME, double timeDelta, double printDelta, int method) {

        particles = new ArrayList<>();
        ForceCalculator f = initializeOscilator();

        double gamma = 100;
        double k = Math.pow(10, 4);
        particles.add(new Particle(1, 0, (-1) * gamma / (2 * 70), 0, 0, 70));

        IntegrationScheme integration = null;
        switch (method) {
            case 0:
                integration = new ExactOscillation(gamma, k);
                break;
            case 1:
                integration = new Gear(particles.get(0), k, gamma);

//                integration = new Beeman(gamma, k);
                break;
            case 2:
                integration = new LeapFrogMethod();
                break;
            case 3:
                integration = new EulerMethod();
                break;
            case 4:
                integration = new Gear(particles.get(0), k, gamma);
                break;
            default:
                System.out.println(method);
        }


        List<Vector> forces = new ArrayList<>();
        for (Particle p : particles) {
            forces.add(new Vector(0, 0));
        }

        List<Vector> positions = new ArrayList<>();

        double timerCounter = 0;
        for (double totalTime = 0; totalTime < MAX_TIME; totalTime += timeDelta) {

            f.calculateForce(particles, forces);

            if (method == 0) {
                integration.updateParticle(particles.get(0), totalTime, 0, 0);
            } else {
                for (int i = 0; i < particles.size(); i++) {
                    Vector newForce = forces.get(i);
                    integration.updateParticle(particles.get(i), timeDelta, newForce.getX(), newForce.getY());
                }
            }


            timerCounter += timeDelta;
            if (timerCounter >= printDelta) {
                positions.add(new Vector(totalTime, particles.get(0).getX()));
                timerCounter = 0;
            }
        }

        return positions;
    }

}
