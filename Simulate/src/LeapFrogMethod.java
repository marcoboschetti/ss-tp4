import java.util.HashMap;
import java.util.Map;

/**
 * Created by tritoon on 19/09/16.
 */
public class LeapFrogMethod implements IntegrationScheme {

    private Map<Integer,Vector> previusHalfVel;

    public LeapFrogMethod() {
        previusHalfVel = new HashMap<>();
    }

    @Override
    public void updateParticle(Particle toUpdate, double deltaT, double forceX, double forceY) {
        Vector previousIHalfVel = null;
        if(previusHalfVel.containsKey(toUpdate.getId())){
            previousIHalfVel = previusHalfVel.get(toUpdate.getId());
        }else{
            Particle previus = toUpdate.clone();
            new EulerMethod().updateParticle(previus,(-1)*deltaT,forceX,forceY);
            previousIHalfVel = new Vector(previus.getVelX(),previus.getVelY());
            previusHalfVel.put(toUpdate.getId(),previousIHalfVel);
        }

        double newHalfVelX = previousIHalfVel.getX() + deltaT * forceX / toUpdate.getMass();
        double newHalfVelY = previousIHalfVel.getY() + deltaT * forceY / toUpdate.getMass();

        double newX = toUpdate.getX() + deltaT * previousIHalfVel.getX();
        double newY = toUpdate.getY() + deltaT * previousIHalfVel.getY();

        toUpdate.setX(newX);
        toUpdate.setY(newY);

        toUpdate.setVelX((newHalfVelX+previousIHalfVel.getX())/2);
        toUpdate.setVelY((newHalfVelY+previousIHalfVel.getY())/2);

        previousIHalfVel.setX(newHalfVelX);
        previousIHalfVel.setY(newHalfVelY);

    }

}
