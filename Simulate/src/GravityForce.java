
import java.util.List;

/**
 * Created by tritoon on 19/09/16.
 */
public class GravityForce implements ForceCalculator {

    private static final double GravityConstant = 6.693 * Math.pow(10, -11);

    @Override
    public void calculateForce(List<Particle> p, List<Vector> forces) {

        for(Vector f:forces){
            f.setX(0);
            f.setY(0);
        }

        for (int i = 0; i < p.size(); i++) {// TODO: 26/9/2016 probar esto 
            Particle p1 = p.get(i);
            for (int j = i + 1; j < p.size(); j++) {
                Particle p2 = p.get(j);

                double distance2 = Math.pow(p1.getX()-p2.getX(),2) +
                        Math.pow(p1.getY() - p2.getY(),2);
                double distance = Math.sqrt(distance2);
                double force = GravityConstant * p1.getMass() * p2.getMass() / (distance2);

//                double fx = - force * (p1.getX() - p2.getX())/ Math.sqrt(distance2);
//                double fy = - force * (p1.getY() - p2.getY())/ Math.sqrt(distance2);

//                double alpha = Math.atan2(p1.getY()-p2.getY(), p1.getX()-p2.getX());
//                double versorX = Math.cos(alpha);
//                double versorY = Math.sin(alpha);

                if(!(p2 instanceof SpaceShip)){
//                    forces.get(i).setX(forces.get(i).getX() - force * versorX);
//                    forces.get(i).setY(forces.get(i).getY() - force * versorY);
                    forces.get(i).setX(forces.get(i).getX() - force * (p1.getX() - p2.getX()) / distance);
                    forces.get(i).setY(forces.get(i).getY() - force * (p1.getY() - p2.getY()) / distance);
                }
                if(!(p1 instanceof SpaceShip)){
//                    forces.get(j).setX(forces.get(j).getX() + force * versorX);
//                    forces.get(j).setY(forces.get(j).getY() + force * versorY);
                    forces.get(j).setX(forces.get(j).getX() + force * (p1.getX() - p2.getX()) / distance);
                    forces.get(j).setY(forces.get(j).getY() + force * (p1.getY() - p2.getY()) / distance);
                }
            }
        }



    }


    private double distance(Particle current, Particle particleJ) {
        double difX = current.getX() - particleJ.getX();
        double difY = current.getY() - particleJ.getY();
        return Math.sqrt(difX * difX + difY * difY);
    }

}
