import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * Created by tritoon on 10/08/16.
 */
public class Main {

    private static final double GravityConstant = 6.693 * Math.pow(10, -11);

    public static void main(String[] args) throws IOException {

        final ChronoUnit durationUnit = ChronoUnit.YEARS,
                offsetUnit = ChronoUnit.SECONDS,
                launchIntervalUnit = ChronoUnit.WEEKS;
        final double MAX_TIME = 9 * 30 * 24 * 60 * 60;
        final int duration = 8, offset = 0, launchInterval = 1;
        double timeDelta = 100;
        String fileXYZ = String.format("%d%sEvery%d%safter%d%supdate%f",
                duration,
                durationUnit.toString(),
                launchInterval,
                launchIntervalUnit.toString(),
                offset,
                offsetUnit.toString(),
                timeDelta);
        double printDelta = 20 * 500;

        //getOscilationStadistics();

        System.out.println("8yearsEvery4day1");
        List<double[]> ans = new Simulator().simulate(MAX_TIME, timeDelta, printDelta, ChronoUnit.HOURS, 8, ChronoUnit.MONTHS, 17, ChronoUnit.DAYS, 0, fileXYZ);

        PrintWriter writer = null;

//        try {
//            writer = new PrintWriter(fileXYZ+"Arrivals.csv", "UTF-8");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        writer.write("Landings\nDeparture,Arrival,Min distance to Mars\n");
//        for(double[] d :ans){
//            writer.write(d[0] + "," + d[1] + "," + d[2] + "," + d[3] + "," + d[4] + "\n");
//        }
//        writer.close();


        //getOscilationStadistics();

    }


    private static List<double[]> singleTest(double MAX_TIME, double timeDelta, double printDelta, String fileXYZ) {
        return new Simulator().simulate(MAX_TIME, timeDelta, printDelta, ChronoUnit.HOURS, 1, ChronoUnit.DAYS, 0, ChronoUnit.DAYS, 0, fileXYZ);
    }
// 3 km/s -> 8  m = 2x10^5 d = 1500 * 1000 7.21 km/s r=6371 * 1000
    private static void test(){
//        Particle current = new Particle(0,0,0,0, 6371 * 1000,5.972 * Math.pow(10, 24)), particleJ = new Particle(1500 * 1000 + 6371 * 1000, 0, 0, 0, 0, 2 * 10^5);
        Particle earth = new Particle(1.391734353396533 * Math.pow(10, 8) * 1000, -0.571059040560652 * Math.pow(10, 8) * 1000,
                10.801963811159256 * 1000, 27.565215006898345 * 1000, 6371 * 1000, 5.972 * Math.pow(10, 24));

        /* SUN
       X			Y			Z
        0			0			0
        VX			VY			VZ
        0			0			0

        mean radius: 695700 km
        mass: 1.988E30 kg
        */
        Particle sun = new Particle(0, 0, 0, 0, 695700 * 1000, 1.988 * Math.pow(10, 30));
        Particle current = earth, particleJ = sun;

        double distance2 = Math.pow(current.getX()-particleJ.getX(),2) +
                Math.pow(current.getY() - particleJ.getY(),2);

        double force = GravityConstant * current.getMass() * particleJ.getMass() / (distance2);
        System.out.println(force/particleJ.getMass());
//                double alpha = Math.acos((current.getX()-particleJ.getX())/Math.sqrt(distance2));
        double alpha = Math.atan2(current.getY()-particleJ.getY(), current.getX()-particleJ.getX());
        double versorX = Math.cos(alpha);
        double versorY = Math.sin(alpha);

    }

    public static void getOscilationStadistics() {

        double MAX_TIME = 5;
        double timeDelta = 0.0000001;
        double printDelta = 0.1;

//        String[] methods = new String[]{"Exact", "Beeman", "Leapfrog", "Euler", "Gear"};
        String[] methods = new String[]{"Exact", "Gear"};

        PrintWriter writer = null;

        try {
            writer = new PrintWriter("OscilationStadistics.csv", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.write("Position\nTime,");

        List<Vector>[] results = new List[5];
        for (int i = 0; i < methods.length; i++) {
            writer.write(String.format("%s method,", methods[i]));
            List<Vector> positions = new Simulator().compareOscillation(MAX_TIME, timeDelta, printDelta, i);
            results[i] = positions;
        }

        writer.write("\n");

        for (int t = 0; t < results[0].size(); t++) {
           writer.write(results[0].get(t).getX()+"");
            for (int i = 0; i < methods.length; i++) {
                writer.write(","+results[i].get(t).getY() );
            }
            writer.write("\n");
        }


        writer.close();


        try {
            writer = new PrintWriter("OscilationError.csv", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        writer.write("Cuadratic Error\nTime,");
        for (int i = 1; i < methods.length; i++) {
            writer.write(String.format("%s method,", methods[i]));
        }
        writer.write("\n");

        for (int t = 0; t < results[0].size(); t++) {
            writer.write(results[0].get(t).getX()+",");
            for (int i = 1; i < methods.length; i++) {
                writer.write(Math.pow(results[0].get(t).getY() - results[i].get(t).getY(),2)/results[0].size()+"," );
            }
            writer.write("\n");
        }


        writer.close();


    }
}