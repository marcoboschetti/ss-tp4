import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by juanc on 27/9/2016.
 */
public class StatisticsParser {
    public static void main(String[] args) throws IOException {

        Reader in = new FileReader("path/to/file.csv");
        Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
        List<CSVRecord> recordList = new LinkedList<>();
        for (CSVRecord record : records) {
            recordList.add(record);
        }


    }
}
