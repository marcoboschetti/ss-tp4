import java.util.List;

/**
 * Created by tritoon on 19/09/16.
 */
public interface ForceCalculator {
    public void calculateForce(List<Particle> p, List<Vector> forces) ;
}
