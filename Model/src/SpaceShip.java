/**
 * Created by juanc on 21/9/2016.
 */
public class SpaceShip extends Particle {
    private double launchTime;
    private double minDistanceToMars;
    private double landingAproxTime;
    private double angle;

    public double getLandingAproxTime() {
        return landingAproxTime;
    }

    public SpaceShip(double x, double y, double velx, double vely, double launchTime, double curentDistance, double angle) {
        super(x, y, velx, vely, 5, 2 * 100000);
        this.angle = angle;
        this.launchTime = launchTime;
        this.landingAproxTime = 0;
        this.minDistanceToMars = curentDistance;
//        System.out.println(curentDistance);
//        System.out.println();
    }

    public double getLaunchTime() {
        return launchTime;
    }

    public void updateDistance(Particle mars, double currentTime) {
        double squareDistanceToMars = (this.getX() - mars.getX()) * (this.getX() - mars.getX()) + (this.getY() - mars.getY()) * (this.getY() - mars.getY());

        if (squareDistanceToMars < minDistanceToMars) {
            minDistanceToMars = squareDistanceToMars;
            landingAproxTime = currentTime;
        }
    }

    public double getMinDistanceToMars() {
        return minDistanceToMars;
    }

    public double getLaunchAngle() {
        return angle;
    }
}
