/**
 * Created by juan on 09/09/16.
 */
public class DataEntry {
    private final double time;
    private final double fp;

    public DataEntry(double time, double fp) {
        this.time = time;
        this.fp = fp;
    }

    public double getTime() {
        return time;
    }

    public double getFp() {
        return fp;
    }
}
